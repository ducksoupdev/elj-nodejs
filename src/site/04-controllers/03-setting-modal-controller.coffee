"use strict"
window.app = window.app or {}
window.app.site.controller "SettingModalController", [
  "$scope"
  "$modalInstance"
  "activeSetting"
  ($scope, $modalInstance, activeSetting) ->
    $scope.cancel = ->
      $modalInstance.dismiss("cancel")
      return

    $scope.isFormValid = (formController) -> formController.$valid

    $scope.save = ->
      $modalInstance.close(activeSetting)
      return

]