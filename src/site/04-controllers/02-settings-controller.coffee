###
Created by Matt Levy on 05/03/14.
###
"use strict"
window.app = window.app or {}
window.app.site.controller "SettingsController", [
  "$scope"
  "$modal"
  "sessionService"
  "settingsService"
  ($scope, $modal, sessionService, settingsService) ->
    sessionService.refresh()
    $scope.settingId = sessionService.getProperty("settingId")
    $scope.activeSetting = null
    settingsService.getSettings (results) ->
      $scope.settings = results
      return

    $scope.editSetting = (setting) ->
      $modal.open({
        templateUrl: "site/partials/setting-modal.html"
        controller: "SettingModalController"
        windowClass: "setting-modal"
        resolve: {
          activeSetting: -> setting
        }
      })
      return

    $scope.save = ->
      # save the setting
      settingsService.save $scope.activeSetting, (results) ->
        $scope.activeSetting = undefined
        return

      $scope.isOpen = false
      return

    return
]
