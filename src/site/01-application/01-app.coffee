###
Created by Matt Levy on 01/04/14.
###
"use strict"
window.app = window.app or {}
window.app.site = angular.module("siteApp", [
  "global"
  "ngRoute"
  "ngResource"
  "ngCookies",
  "ui.bootstrap"
]).config([
  "$routeProvider"
  ($routeProvider) ->
    
    # routes 
    $routeProvider.when "/home",
      templateUrl: "site/partials/home.html"
      controller: "HomeController"

    $routeProvider.when "/settings",
      templateUrl: "site/partials/settings.html"
      controller: "SettingsController"

    $routeProvider.otherwise redirectTo: "/home"
])
