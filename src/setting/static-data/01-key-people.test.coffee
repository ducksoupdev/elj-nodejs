###
Created by Matt Levy on 18/03/14.
###
"use strict"
window.app = window.app or {}
window.app.setting.testKeyPeople = ->
  getKeyPeople: (settingId) ->
    filteredSettings = []
    angular.forEach @testKeyPeople, (keyPerson) ->
      filteredSettings.push keyPerson  if keyPerson.settingId is settingId
      return

    filteredSettings

  getKeyPerson: (keyPersonId) ->
    allKeyPeopleDetails = @testKeyPeople
    app.arraySearch().single allKeyPeopleDetails, (keyPersonDetail) ->
      keyPersonId is keyPersonDetail.keyPersonId


  testKeyPeople: [
    {
      keyPersonId: "A8E257F6-0649-0F45-A0A2-D275279B7384"
      name: "Keane Horton"
      notes: "Lorem ipsum dolor sit amet,"
      status: "Active"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      loginId: "0FFED851-8ADE-ECB9-3B15-DB6C3C8C5A1A"
      photoId: "DB6A8DE0-7C5B-BF38-600D-9B29124EAD1E"
    }
    {
      keyPersonId: "F8C85D6D-82E0-6BBD-BAFF-505A18EEED4B"
      name: "Samuel Rich"
      notes: "Lorem"
      status: "Active"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      loginId: "758299D0-C4A9-F77F-1209-90C6038654E6"
      photoId: "F8D46A34-3D35-A58C-5812-A4502DC3E330"
    }
    {
      keyPersonId: "AEBA99F1-36FC-49FF-ECB0-90C95D2C2AEE"
      name: "Ira Harmon"
      notes: "Lorem ipsum"
      status: "Active"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      loginId: "F971DD35-43F7-DF28-72C8-EAADA087C87E"
      photoId: "CF51AF4F-366A-024E-992A-E3FEEC469EC5"
    }
    {
      keyPersonId: "C4B29745-FC39-771F-1640-4F4047A4E435"
      name: "Peter Higgins"
      notes: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed"
      status: "Active"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      loginId: "14B57693-F342-E8F4-6E9D-7BCCEC99A1C2"
      photoId: "86152CD9-CB2A-4319-E6BC-99E3218F7D77"
    }
    {
      keyPersonId: "5BEEF4B1-EF27-9764-239A-8B1FBD88D9E8"
      name: "Ryder Mccall"
      notes: "Lorem ipsum dolor sit amet,"
      status: "Active"
      settingId: "06BCEB3B-AA5B-A832-1B1B-8E968F72A65A"
      loginId: "E4B75F1E-DDCE-EA42-DBB0-8ED3B53F8EE9"
      photoId: "488CB868-7B01-1FCD-83AA-FE13CEFA4508"
    }
    {
      keyPersonId: "F1306017-5A4E-0479-D464-03A87884D56B"
      name: "Orlando Mccarthy"
      notes: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed"
      status: "Active"
      settingId: "900E7940-19DB-F7A4-AEE5-1815DA340352"
      loginId: "15374154-23F1-0856-181D-D28ED31FC3DC"
      photoId: "708C6DF1-EE44-3F02-AAA8-D5A490AA6B1F"
    }
    {
      keyPersonId: "79000BD6-2301-B908-0277-F9719A707011"
      name: "Hyatt Marks"
      notes: "Lorem ipsum dolor"
      status: "Active"
      settingId: "9389E840-626E-804B-FE98-E9C33BC51C7E"
      loginId: "E56FBAB2-9D4A-BA0E-1A91-FCC2A5099487"
      photoId: "E997D454-A7BF-3B3D-B567-CCBBFF7D945F"
    }
    {
      keyPersonId: "66C0FD5D-F974-46C4-ADE5-DE2C8E5E2A34"
      name: "Hakeem Key"
      notes: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur"
      status: "Active"
      settingId: "B4C241C9-E2D0-BDB1-D284-1C812A543DBD"
      loginId: "61F9248D-38D4-94EC-A913-A8486A995C54"
      photoId: "FD0BE8A9-4D3F-5D26-C9D7-12F13F18A016"
    }
    {
      keyPersonId: "D911DCF8-1C2B-8320-47F2-7C830A11FDE8"
      name: "Joseph Goodwin"
      notes: "Lorem"
      status: "Active"
      settingId: "91B550FE-3A4F-E7D8-BDB0-0605EB0EC241"
      loginId: "DA00E7C0-30D0-01DE-DDB1-4CB1D6109FAE"
      photoId: "BABE55B3-EE57-236A-2917-34A3AEB3984C"
    }
    {
      keyPersonId: "280E480B-C5D6-3668-3EC2-4CA6CDDF7251"
      name: "Calvin Watts"
      notes: "Lorem ipsum dolor"
      status: "Active"
      settingId: "6EC9B12F-9664-F8D7-129D-3234C5299A06"
      loginId: "B6A3978C-45A3-7A7C-E8A3-4ADDC5397ACB"
      photoId: "05F97A06-66B5-66DB-AD7C-61DCD837EB4A"
    }
    {
      keyPersonId: "B4D25471-FA4A-34DA-D7AA-DD2B0965EECA"
      name: "Quentin Taylor"
      notes: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur"
      status: "Active"
      settingId: "6689F0EF-5AC4-9D2A-0560-B0503850CD2A"
      loginId: "D3187580-8B27-18BF-D602-68B269F47F88"
      photoId: "42FCB1C0-A7D5-B43F-C549-98F2D2C366B3"
    }
    {
      keyPersonId: "5F91277F-97CA-14A7-91DF-9DEB74F79671"
      name: "George Medina"
      notes: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit."
      status: "Active"
      settingId: "AA2D9667-1472-4F01-F10C-7D5CEF0ED267"
      loginId: "EB4BACE8-60F1-4BA0-CA41-626172468C8A"
      photoId: "63EB368F-9B8B-1078-82F0-D673C17474F9"
    }
    {
      keyPersonId: "F6BC67EA-BA95-D52E-8BEC-530A3FF39994"
      name: "Cairo Garrison"
      notes: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur"
      status: "Active"
      settingId: "0F267C6E-2040-0DEF-8769-39D70B743CD1"
      loginId: "89AA86D0-786C-75E7-2CBE-9C587882646C"
      photoId: "113DA106-FA2D-E952-1D46-BB4FB8B3F574"
    }
    {
      keyPersonId: "1E47EF96-0D8A-806E-5290-CB9288B2199C"
      name: "Herman Chaney"
      notes: "Lorem ipsum dolor"
      status: "Active"
      settingId: "91B28451-1B01-27D6-8F40-A358B27C590A"
      loginId: "C5E7828F-42BA-5B69-853B-B171CD61877F"
      photoId: "B07537EE-EB1B-3C4C-32AA-32DA04803A78"
    }
    {
      keyPersonId: "3AE640CA-D943-8FD0-9659-5C7033D1BDD3"
      name: "Erasmus Mccoy"
      notes: "Lorem ipsum"
      status: "Active"
      settingId: "4A388A38-604A-5B8D-EE4C-662ECCA93651"
      loginId: "AEC2AEE0-0847-FC93-21FE-0C24447E4618"
      photoId: "8836CDE1-7DD9-D07A-73A4-0894B66A32CD"
    }
    {
      keyPersonId: "89D32088-B307-0B1C-53E1-D7BC0051523E"
      name: "Norman Buchanan"
      notes: "Lorem ipsum dolor sit amet,"
      status: "Active"
      settingId: "8B74C535-618B-F5A6-F874-5D5CB56CBB2A"
      loginId: "9B6F705E-8711-7A94-A428-F5F79976722D"
      photoId: "9C51B062-9C77-A99B-37FE-9218934D8251"
    }
    {
      keyPersonId: "BD059C34-F410-7C3A-C53C-813D9ADB31C8"
      name: "Prescott Jennings"
      notes: "Lorem"
      status: "Active"
      settingId: "A44F6EF4-5110-85E2-9461-282D71240D0E"
      loginId: "2E9D5FB5-B24E-7F75-9821-A421CF38C916"
      photoId: "3E740A9C-1966-5935-C7DB-D7069B010C70"
    }
    {
      keyPersonId: "6DEBE0EE-79D9-4D36-3263-B1A8277EA9E1"
      name: "Jin Daugherty"
      notes: "Lorem ipsum dolor"
      status: "Active"
      settingId: "631B6DA4-84D5-0812-85C3-CE456371760A"
      loginId: "CF894BDA-9242-5337-6B97-D14F32F2F5C7"
      photoId: "C63DCD13-2447-60F2-F521-47487D3A7088"
    }
    {
      keyPersonId: "4A5BB6DA-AE9D-9F5E-7DD1-4E439DCCA6A0"
      name: "Xanthus Stephens"
      notes: "Lorem"
      status: "Active"
      settingId: "23F4BFB1-472C-90D6-8505-CFE2571AA460"
      loginId: "A7E4A929-AAE2-532C-B52B-D3733C514A09"
      photoId: "8EE2E063-00E6-4AF2-B890-0CC22047ADD2"
    }
    {
      keyPersonId: "1699E4E3-D875-32DB-DD25-99830C752495"
      name: "Xavier Thornton"
      notes: "Lorem ipsum dolor sit amet, consectetuer"
      status: "Active"
      settingId: "53865586-9588-61F6-C520-E9E93C5DEFFF"
      loginId: "FD68369A-9DB4-ED2A-12BE-B4750AAD1F11"
      photoId: "3AB32791-C030-4AAB-2870-3D6B823146A3"
    }
  ]
