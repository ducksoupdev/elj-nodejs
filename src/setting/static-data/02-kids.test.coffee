###
Created by Matt Levy on 18/03/14.
###
"use strict"
window.app = window.app or {}
window.app.setting.testKids = ->
  getKids: (settingId) ->
    filteredSettings = []
    angular.forEach @testKids, (kid) ->
      filteredSettings.push kid  if kid.settingId is settingId
      return

    filteredSettings

  getKid: (kidId) ->
    allKidsDetails = @testKids
    app.arraySearch().single allKidsDetails, (kidDetail) ->
      kidId is kidDetail.kidId


  testKids: [
    {
      kidId: "A8E257F6-0649-0F45-A0A2-D275279B7384"
      name: "Keane Horton"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      photoId: "DB6A8DE0-7C5B-BF38-600D-9B29124EAD1E"
    }
    {
      kidId: "F8C85D6D-82E0-6BBD-BAFF-505A18EEED4B"
      name: "Samuel Rich"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      photoId: "F8D46A34-3D35-A58C-5812-A4502DC3E330"
    }
    {
      kidId: "AEBA99F1-36FC-49FF-ECB0-90C95D2C2AEE"
      name: "Ira Harmon"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      photoId: "CF51AF4F-366A-024E-992A-E3FEEC469EC5"
    }
    {
      kidId: "C4B29745-FC39-771F-1640-4F4047A4E435"
      name: "Peter Higgins"
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      photoId: "86152CD9-CB2A-4319-E6BC-99E3218F7D77"
    }
    {
      kidId: "5BEEF4B1-EF27-9764-239A-8B1FBD88D9E8"
      name: "Ryder Mccall"
      settingId: "06BCEB3B-AA5B-A832-1B1B-8E968F72A65A"
      photoId: "488CB868-7B01-1FCD-83AA-FE13CEFA4508"
    }
    {
      kidId: "F1306017-5A4E-0479-D464-03A87884D56B"
      name: "Orlando Mccarthy"
      settingId: "900E7940-19DB-F7A4-AEE5-1815DA340352"
      photoId: "708C6DF1-EE44-3F02-AAA8-D5A490AA6B1F"
    }
    {
      kidId: "79000BD6-2301-B908-0277-F9719A707011"
      name: "Hyatt Marks"
      settingId: "9389E840-626E-804B-FE98-E9C33BC51C7E"
      photoId: "E997D454-A7BF-3B3D-B567-CCBBFF7D945F"
    }
    {
      kidId: "66C0FD5D-F974-46C4-ADE5-DE2C8E5E2A34"
      name: "Hakeem Key"
      settingId: "B4C241C9-E2D0-BDB1-D284-1C812A543DBD"
      photoId: "FD0BE8A9-4D3F-5D26-C9D7-12F13F18A016"
    }
    {
      kidId: "D911DCF8-1C2B-8320-47F2-7C830A11FDE8"
      name: "Joseph Goodwin"
      settingId: "91B550FE-3A4F-E7D8-BDB0-0605EB0EC241"
      photoId: "BABE55B3-EE57-236A-2917-34A3AEB3984C"
    }
    {
      kidId: "280E480B-C5D6-3668-3EC2-4CA6CDDF7251"
      name: "Calvin Watts"
      settingId: "6EC9B12F-9664-F8D7-129D-3234C5299A06"
      photoId: "05F97A06-66B5-66DB-AD7C-61DCD837EB4A"
    }
    {
      kidId: "B4D25471-FA4A-34DA-D7AA-DD2B0965EECA"
      name: "Quentin Taylor"
      settingId: "6689F0EF-5AC4-9D2A-0560-B0503850CD2A"
      photoId: "42FCB1C0-A7D5-B43F-C549-98F2D2C366B3"
    }
    {
      kidId: "5F91277F-97CA-14A7-91DF-9DEB74F79671"
      name: "George Medina"
      settingId: "AA2D9667-1472-4F01-F10C-7D5CEF0ED267"
      photoId: "63EB368F-9B8B-1078-82F0-D673C17474F9"
    }
    {
      kidId: "F6BC67EA-BA95-D52E-8BEC-530A3FF39994"
      name: "Cairo Garrison"
      settingId: "0F267C6E-2040-0DEF-8769-39D70B743CD1"
      photoId: "113DA106-FA2D-E952-1D46-BB4FB8B3F574"
    }
    {
      kidId: "1E47EF96-0D8A-806E-5290-CB9288B2199C"
      name: "Herman Chaney"
      settingId: "91B28451-1B01-27D6-8F40-A358B27C590A"
      photoId: "B07537EE-EB1B-3C4C-32AA-32DA04803A78"
    }
    {
      kidId: "3AE640CA-D943-8FD0-9659-5C7033D1BDD3"
      name: "Erasmus Mccoy"
      settingId: "4A388A38-604A-5B8D-EE4C-662ECCA93651"
      photoId: "8836CDE1-7DD9-D07A-73A4-0894B66A32CD"
    }
    {
      kidId: "89D32088-B307-0B1C-53E1-D7BC0051523E"
      name: "Norman Buchanan"
      settingId: "8B74C535-618B-F5A6-F874-5D5CB56CBB2A"
      photoId: "9C51B062-9C77-A99B-37FE-9218934D8251"
    }
    {
      kidId: "BD059C34-F410-7C3A-C53C-813D9ADB31C8"
      name: "Prescott Jennings"
      settingId: "A44F6EF4-5110-85E2-9461-282D71240D0E"
      photoId: "3E740A9C-1966-5935-C7DB-D7069B010C70"
    }
    {
      kidId: "6DEBE0EE-79D9-4D36-3263-B1A8277EA9E1"
      name: "Jin Daugherty"
      settingId: "631B6DA4-84D5-0812-85C3-CE456371760A"
      photoId: "C63DCD13-2447-60F2-F521-47487D3A7088"
    }
    {
      kidId: "4A5BB6DA-AE9D-9F5E-7DD1-4E439DCCA6A0"
      name: "Xanthus Stephens"
      settingId: "23F4BFB1-472C-90D6-8505-CFE2571AA460"
      photoId: "8EE2E063-00E6-4AF2-B890-0CC22047ADD2"
    }
    {
      kidId: "1699E4E3-D875-32DB-DD25-99830C752495"
      name: "Xavier Thornton"
      settingId: "53865586-9588-61F6-C520-E9E93C5DEFFF"
      photoId: "3AB32791-C030-4AAB-2870-3D6B823146A3"
    }
  ]
