###
Created by Matt Levy on 05/03/14.
###
"use strict"
window.app = window.app or {}
window.app.setting.controller "KidsController", [
  "$scope"
  "sessionService"
  "kidsService"
  ($scope, sessionService, kidsService) ->
    sessionService.refresh()
    $scope.settingId = sessionService.getProperty("settingId")
    $scope.isOpen = false
    $scope.activeKid = null
    kidsService.getKids $scope.settingId, (results) ->
      $scope.kids = results
      return

    $scope.editKid = (kid) ->
      $scope.activeKid = kid
      $scope.isOpen = true
      return

    $scope.save = ->
      # save the kid
      kidsService.save $scope.activeKid, (results) ->
        $scope.activeKid = undefined
        return

      $scope.isOpen = false
      return

    return
]
