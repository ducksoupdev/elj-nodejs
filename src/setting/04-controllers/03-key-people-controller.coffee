###
Created by Matt Levy on 05/03/14.
###
"use strict"
window.app = window.app or {}
window.app.setting.controller "KeyPeopleController", [
  "$scope"
  "sessionService"
  "keyPeopleService"
  ($scope, sessionService, keyPeopleService) ->
    sessionService.refresh()
    $scope.settingId = sessionService.getProperty("settingId")
    $scope.isOpen = false
    $scope.activeKeyPerson = null
    keyPeopleService.getKeyPeople $scope.settingId, (results) ->
      $scope.keyPeople = results
      return

    $scope.editKeyPerson = (keyPerson) ->
      $scope.activeKeyPerson = keyPerson
      $scope.isOpen = true
      return

    $scope.save = ->
      # save the key person
      keyPeopleService.save $scope.activeKeyPerson, (results) ->
        $scope.activeKeyPerson = undefined
        return

      $scope.isOpen = false
      return

    return
]
