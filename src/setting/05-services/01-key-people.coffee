###
Created by Matt Levy on 13/03/14.
###
"use strict"
window.app = window.app or {}
window.app.setting.factory "keyPeopleService", [
  "$resource"
  "globalServiceUris"
  "eljRuntime"
  ($resource, globalServiceUris, eljRuntime) ->
    return (
      getKeyPeople: (settingId, callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          keyPeopleResource = $resource(globalServiceUris.keyPeopleUri)
          result = keyPeopleResource.query(
            settingId: settingId
          , ->
            callback result
            return
          )
        else
          result = window.app.setting.testKeyPeople().getKeyPeople(settingId)
          callback result
        return

      getKeyPerson: (keyPersonId, callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          keyPersonResource = $resource(globalServiceUris.keyPersonUri)
          result = keyPersonResource.get(
            keyPersonId: keyPersonId
          , ->
            callback result
            return
          )
        else
          result = window.app.setting.testKeyPeople().getKeyPerson(keyPersonId)
          callback result
        return

      save: (keyPerson, callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          keyPersonResource = $resource(globalServiceUris.keyPersonUri)
          result = keyPersonResource.save(
            keyPersonId: keyPerson.keyPersonId,
            keyPerson
          , ->
            callback result
            return
          )
        else
          callback keyPerson
        return
    )
]
