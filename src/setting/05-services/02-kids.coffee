###
Created by Matt Levy on 13/03/14.
###
"use strict"
window.app = window.app or {}
window.app.setting.factory "kidsService", [
  "$resource"
  "globalServiceUris"
  "eljRuntime"
  ($resource, globalServiceUris, eljRuntime) ->
    return (
      getKids: (settingId, callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          kidsResource = $resource(globalServiceUris.kidsUri)
          result = kidsResource.query(
            settingId: settingId
          , ->
            callback result
            return
          )
        else
          result = window.app.setting.testKids().getKids(settingId)
          callback result
        return

      getKid: (kidId, callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          kidResource = $resource(globalServiceUris.kidUri)
          result = kidResource.get(
            kidId: kidId
          , ->
            callback result
            return
          )
        else
          result = window.app.setting.testKids().getKid(kidId)
          callback result
        return

      save: (kid, callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          kidResource = $resource(globalServiceUris.kidUri)
          result = kidResource.save(
            kidId: kid.kidId,
            kid
          , ->
            callback result
            return
          )
        else
          callback kid
        return
    )
]
