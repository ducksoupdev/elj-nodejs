###
Created by Matt Levy ducksoupdev@gmail.com on 01/04/14.
###
"use strict"
window.app = window.app or {}
window.app.setting = angular.module("settingApp", [
  "global"
  "ngRoute"
  "ngResource"
  "ngCookies"
]).config([
  "$routeProvider"
  ($routeProvider) ->
    
    # routes 
    $routeProvider.when "/home",
      templateUrl: "setting/partials/home.html"
      controller: "HomeController"

    $routeProvider.when "/keyPeople",
      templateUrl: "setting/partials/keyPeople.html"
      controller: "KeyPeopleController"

    $routeProvider.when "/kids",
      templateUrl: "setting/partials/kids.html"
      controller: "KidsController"

    $routeProvider.otherwise redirectTo: "/home"
])
