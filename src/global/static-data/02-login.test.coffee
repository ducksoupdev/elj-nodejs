###
Created by levym on 13/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.testLogin = ->
  getLogin: (loginId) ->
    allLoginDetails = @testLoginDetails
    app.arraySearch().single allLoginDetails, (loginDetail) ->
      loginId is loginDetail.loginId


  testLoginDetails: [
    {
      loginId: "0FFED851-8ADE-ECB9-3B15-DB6C3C8C5A1A"
      username: "dolor@magnaCrasconvallis.com"
      password: "BQD04REO1QF"
      usernameIsEmail: "true"
      verificationCode: "YIN45YHM8PD"
      verifiedAt: "2014-04-18 22:32:31"
      entity: "Setting"
      entityId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
    {
      loginId: "D7704F88-4AB5-CCD9-34C6-9D924D9FC64D"
      username: "Aenean.euismod.mauris@Vestibulumanteipsum.org"
      password: "QJC52HLL6NI"
      usernameIsEmail: "false"
      verificationCode: "RWN12FRP2AG"
      verifiedAt: "2014-12-11 06:11:21"
      entity: "Setting"
      entityId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
    {
      loginId: "23125776-EF66-7D27-E412-32E148E373D8"
      username: "Sed.eu.nibh@acsemut.org"
      password: "JFF46SSN5VA"
      usernameIsEmail: "true"
      verificationCode: "OOG03PVW2ZY"
      verifiedAt: "2013-10-27 17:09:20"
      entity: "KeyPerson"
      entityId: "E4198B39-E687-F694-26B7-43FEE4EC2BB1"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
    {
      loginId: "3EC2BE9E-F96D-672B-E195-AF059E327DCA"
      username: "nunc.sed@Vivamuseuismodurna.ca"
      password: "CWC51EMS5IU"
      usernameIsEmail: "false"
      verificationCode: "ZHW98ZZP7NG"
      verifiedAt: "2013-04-02 23:35:03"
      entity: "KeyPerson"
      entityId: "55C98141-19D1-6BBF-D812-BA73485A8ADE"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
    {
      loginId: "B19CA146-8745-411A-95B3-B651069BC1E0"
      username: "sit.amet.consectetuer@vestibulumneque.ca"
      password: "AHW88YSW3SS"
      usernameIsEmail: "true"
      verificationCode: "KMF34FXT7FI"
      verifiedAt: "2013-08-15 22:54:49"
      entity: "KeyPerson"
      entityId: "48338DA9-AC5A-36A0-737B-CE56C0FB9DA1"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
    {
      loginId: "CF4F4F61-C698-9F1B-9E29-59E33E5668BF"
      username: "mauris.sagittis.placerat@magnaetipsum.co.uk"
      password: "KAE13VMY6SN"
      usernameIsEmail: "false"
      verificationCode: "AAZ68LID5GI"
      verifiedAt: "2013-05-23 11:12:03"
      entity: "Carer"
      entityId: "A7F6742A-3764-A0CA-189F-AC1C3E1E6CBD"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
    {
      loginId: "CCFB2CBB-7AFC-6C0B-F688-11C4DA670F99"
      username: "eu.odio.Phasellus@sedpedeCum.com"
      password: "QKS83MNI0ZB"
      usernameIsEmail: "true"
      verificationCode: "SUS76OQV6LT"
      verifiedAt: "2014-11-17 21:47:53"
      entity: "Carer"
      entityId: "9A5ECCB4-9C77-540B-EDF3-B703792A3998"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
    {
      loginId: "CE32D666-C486-EC03-1954-7CC87F97FA1E"
      username: "ut.pharetra.sed@Crasdolordolor.co.uk"
      password: "TPG69ARI7OL"
      usernameIsEmail: "false"
      verificationCode: "RKB51YGN6YM"
      verifiedAt: "2014-10-21 18:03:23"
      entity: "Carer"
      entityId: "9935E59C-E344-D294-16CE-71CE288123FD"
      loginCount: 0
      lastLoginDate: null
      autoLogin: null
      createdAt: "2014-03-13T18:00:00.511Z"
      updatedAt: "2014-03-13T18:00:00.511Z"
    }
  ]
