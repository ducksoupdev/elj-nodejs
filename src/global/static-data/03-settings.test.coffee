###
Created by levym on 13/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.testSettings = ->
  getSetting: (settingId) ->
    allSettings = @testSettings
    app.arraySearch().single allSettings, (settingDetail) ->
      settingId is settingDetail.settingId

  getSettings: -> @testSettings

  testSettings: [
    {
      settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      name: "Laoreet Ltd"
      phone: "04275 505984"
      urn: 31314
      address: "Ap #126-5038 Tempus St."
      city: "Portsoy"
      postcode: "F2V 2UK"
      county: "BA"
      country: "United Kingdom"
      contactName: "Roary Singleton"
      type: "Full"
      source: "interdum. Sed auctor"
      researchContact: "true"
      shortUrl: "interdum. Sed"
      status: "Active"
      loginId: "773D6664-3823-9E76-BADE-21A2F96043AE"
    }
    {
      settingId: "DF3AD37A-073A-658B-7EA5-2C9A95DB025B"
      name: "Euismod Company"
      phone: "00191 692348"
      urn: 24377
      address: "388-7979 Habitant Street"
      city: "Tain"
      postcode: "OK2 1WV"
      county: "RO"
      country: "United Kingdom"
      contactName: "Jenna Sanders"
      type: "Full"
      source: "ultrices. Duis"
      researchContact: "true"
      shortUrl: "Nullam"
      status: "Active"
      loginId: "78C9B6B6-DFAD-5AF2-E3A9-7C825A2ED811"
    }
    {
      settingId: "317EEE03-DC78-BCF2-F5A4-75F38B161C80"
      name: "Eget Institute"
      phone: "03130 215114"
      urn: 27336
      address: "P.O. Box 892, 9451 Tortor Rd."
      city: "Camborne"
      postcode: "FL0 6JP"
      county: "Cornwall"
      country: "United Kingdom"
      contactName: "Jaquelyn Burks"
      type: "Full"
      source: "Curae; Phasellus ornare."
      researchContact: "true"
      shortUrl: "felis"
      status: "Active"
      loginId: "14DEA9CC-6860-FD8F-0D9A-8CFF69E14F6A"
    }
    {
      settingId: "4B3050F6-9DA9-3590-E254-56886332EBDB"
      name: "Pellentesque A Facilisis Corp."
      phone: "01243 031406"
      urn: 22696
      address: "P.O. Box 215, 4880 Dolor, Rd."
      city: "Greenlaw"
      postcode: "E6I 5YW"
      county: "BE"
      country: "United Kingdom"
      contactName: "Deanna Allison"
      type: "Full"
      source: "dolor"
      researchContact: "true"
      shortUrl: "leo."
      status: "Active"
      loginId: "D3B5ACF0-817C-641F-6F34-1F7EE51AC20F"
    }
    {
      settingId: "4F48B1FF-A310-68A0-06EA-4806621E7CBC"
      name: "Elit Inc."
      phone: "01973 436379"
      urn: 26104
      address: "P.O. Box 664, 8109 Nulla Av."
      city: "Chester"
      postcode: "KO9 5SV"
      county: "CH"
      country: "United Kingdom"
      contactName: "Elaine Carey"
      type: "Full"
      source: "ornare tortor"
      researchContact: "true"
      shortUrl: "consequat nec,"
      status: "Active"
      loginId: "813BF96C-E7DA-2C97-3FE0-30E3556DDAFB"
    }
  ]
