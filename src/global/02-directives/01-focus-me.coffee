###
Created by levym on 06/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.directive "focusMe", [
  "$timeout"
  ($timeout) ->
    return (scope, element, attrs) ->
      className = null
      className = attrs.focusClass  if angular.isDefined(attrs.focusClass)
      scope.$watch attrs.focusMe, (newValue) ->
        if newValue
          element.addClass "focused"  if className # this is to make it testable!
          $timeout ->
            element[0].focus()
            return

        return

      element.bind "blur", (e) ->
        $timeout (->
          scope.$apply attrs.focusMe + "=false"
          element.removeClass "focused" # remove class when not in focus
          return
        ), 0
        return

      element.bind "focus", (e) ->
        $timeout (->
          scope.$apply attrs.focusMe + "=true"
          return
        ), 0
        return

      return
]
