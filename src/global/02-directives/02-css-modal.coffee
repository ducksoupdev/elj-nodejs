###
Created by levym on 27/03/2014.
###
"use strict"
window.app = window.app or {}
window.app.elj.directive "cssModal", [->
  restrict: "E"
  transclude: true
  replace: true
  template: "<section class='modal--show' tabindex='-1' role='dialog' aria-labelledby='label-show' aria-hidden='true'><div ng-transclude></div><a class='modal-close' title='Close this modal' data-dismiss='modal' data-close='Close' data-ng-click='close()'>&times;</a></section>"
  scope:
    isOpen: "="

  link: (scope, element, attrs) ->
    setFocus = ->
      if scope.activeElement
        scope.lastActive = document.activeElement
        scope.activeElement.focus()
      return
    removeFocus = ->
      scope.lastActive.focus()  if scope.lastActive
      return
    scope.activeElement = `undefined`
    scope.lastActive = `undefined`
    element.on "keydown", (event) ->
      # escape key
      scope.$apply -> scope.close() if event.keyCode is 27
      return

    scope.close = ->
      scope.isOpen = false
      return

    scope.$watch "isOpen", (newValue, oldValue) ->
      if newValue isnt oldValue
        if newValue
          element.addClass "is-active"
          scope.activeElement = element[0]
          setFocus()
        else
          element.removeClass "is-active"
          scope.activeElement = null
          removeFocus()
      return

    return
]
