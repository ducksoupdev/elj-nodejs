###
Created by levym on 14/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.factory "loginService", [
  "$resource"
  "globalServiceUris"
  "eljRuntime"
  ($resource, globalServiceUris, eljRuntime) ->
    return getLogin: (loginId, callback) ->
      
      # get the session details
      result = undefined
      if eljRuntime.useLiveData()
        loginResource = $resource(globalServiceUris.loginUri)
        result = loginResource.get(
          loginId: loginId
        , ->
          callback result
          return
        , (response) ->
        )
      
      # TODO: check the response
      else
        result = window.app.elj.testLogin().getLogin(loginId)
        callback result
      return
]
