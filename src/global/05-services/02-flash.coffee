###
Created by levym on 06/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.factory "flashService", [
  "$rootScope"
  ($rootScope) ->
    flashService = {}
    
    # create an array of alerts available globally
    messages = []
    $rootScope.getMessages = ->
      messages.slice 0

    flashService.add = (level, text) ->
      messages.push
        level: level
        text: text

      return

    flashService.clear = ->
      messages = []
      return

    flashService.getMessages = ->
      messages

    $rootScope.$on "$routeChangeSuccess", flashService.clear
    return flashService
]
