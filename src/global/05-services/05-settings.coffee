###
Created by levym on 13/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.factory "settingsService", [
  "$resource"
  "globalServiceUris"
  "eljRuntime"
  ($resource, globalServiceUris, eljRuntime) ->
    return (
      getSetting: (settingId, callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          settingDetailsResource = $resource(globalServiceUris.settingsUri)
          result = settingDetailsResource.get(
            settingId: settingId
          , ->
            callback result
            return
          )
        else
          result = window.app.elj.testSettings().getSetting(settingId)
          callback result
        return

      getSettings: (callback) ->
        result = undefined
        if eljRuntime.useLiveData()
          settingDetailsResource = $resource(globalServiceUris.settingsUri)
          result = settingDetailsResource.query ->
            callback result
            return

        else
          result = window.app.elj.testSettings().getSettings()
          callback result
        return
    )
]
