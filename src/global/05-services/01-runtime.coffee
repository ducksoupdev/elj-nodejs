###
Created by levym on 06/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.factory "eljRuntime", [
  "$location"
  ($location) ->
    return useLiveData: ->
      return false  if $location.protocol() is "file"
      host = $location.host()
      return false  if host is "locahost" or host is "127.0.0.1" or host.match(/^local\.\*+/)
      port = $location.port()
      return false  if port >= 60000 and port <= 69999
      true
]
