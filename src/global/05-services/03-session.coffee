###
Created by levym on 13/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj.factory "sessionService", [
  "$resource"
  "globalServiceUris"
  "eljRuntime"
  ($resource, globalServiceUris, eljRuntime) ->
    sessionService = {}
    sessionService.refresh = ->
      
      # get the session details
      result = undefined
      if eljRuntime.useLiveData()
        sessionDetailsResource = $resource(globalServiceUris.sessionUri)
        result = sessionDetailsResource.get({}, ->
          sessionService.session = result
          return
        , (response) ->
          
          # TODO: check the response?
          sessionService.session = `undefined`
          return
        )
      else
        result = window.app.elj.testSession().getSession()
        sessionService.session = result
      return

    
    # is the session valid
    sessionService.isValid = ->
      return true  if angular.isDefined(sessionService.session)
      false

    
    # gets a session property
    sessionService.getProperty = (name) ->
      sessionService.session[name]

    
    # fetch the session initially
    sessionService.refresh()
    return sessionService
]
