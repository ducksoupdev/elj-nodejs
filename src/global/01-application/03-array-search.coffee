###
Created by Matt Levy on 14/03/14.
###
"use strict"
window.app = window.app or {}
window.app.arraySearch = ->
  single: (array, compareFunc) ->
    return null  unless array
    index = 0

    while index < array.length
      item = array[index]
      return item  if compareFunc(item)
      index++
    null

  all: (array, compareFunc) ->
    return null  unless array
    matches = []
    index = 0

    while index < array.length
      item = array[index]
      matches.push item  if compareFunc(item)
      index++
    matches
