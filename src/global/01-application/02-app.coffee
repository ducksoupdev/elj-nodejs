###
Created by levym on 06/03/14.
###
"use strict"
window.app = window.app or {}
window.app.elj = angular.module("global", ["ngResource"]).value("globalServiceUris",
  sessionUri: window.QUERY_HOST + "/api/v1/session"
  settingsUri: window.QUERY_HOST + "/api/v1/settings/:settingId"
  loginUri: window.QUERY_HOST + "/api/v1/login/:loginId"
  keyPeopleUri: window.QUERY_HOST + "/api/v1/keyPeople/:settingId"
  keyPersonUri: window.QUERY_HOST + "/api/v1/keyPerson/:keyPersonId"
).factory("arraySearch", [->
  new app.arraySearch()
])
