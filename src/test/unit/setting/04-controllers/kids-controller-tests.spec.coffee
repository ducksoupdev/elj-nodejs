###
Created by levym on 19/03/14.
###
"use strict"
describe "kidsController", ->
  kidsController = undefined
  scope = undefined
  mockSessionService = jasmine.createSpyObj("sessionService", [
    "getProperty"
    "refresh"
  ])
  mockSessionService.getProperty.andCallFake (name) ->
    "12345"

  mockKidsService = jasmine.createSpyObj("kidsService", ["getKids", "getKid", "save"])
  mockKidsService.getKids.andCallFake (settingId, callback) ->
    callback [
      {
        settingId: "12345"
        name: "Test 1"
      }
      {
        settingId: "12345"
        name: "Test 2"
      }
      {
        settingId: "12345"
        name: "Test 3"
      }
    ]
    return

  mockKidsService.getKid.andCallFake (kidId, callback) ->
    callback {
      kidId: kidId,
      settingId: "12345",
      name: "Test 1"
    }
    return

  mockKidsService.save.andCallFake (kid, callback) ->
    callback kid
    return

  beforeEach module("settingApp")
  beforeEach module(($provide) ->
    $provide.value "sessionService", mockSessionService
    $provide.value "kidsService", mockKidsService
    return
  )
  beforeEach inject(($controller, $rootScope) ->
    scope = $rootScope.$new()
    kidsController = $controller("KidsController",
      $scope: scope
    )
    return
  )
  it "Should load the kids for the setting", ->
    expect(scope.kids.length).toBe 3
    return

  describe "Editing a kid", ->
    kid = undefined
    beforeEach ->
      kid =
        kidId: "12345"
        name: "Test 1"

      scope.editKid kid
      return

    it "Should set the modal to open", ->
      expect(scope.isOpen).toBeTruthy()
      return

    return

  describe "Saving a kid", ->
    kid = undefined
    beforeEach ->
      kid =
        kidId: "12345"
        name: "Test 1"

      scope.save kid
      return

    it "Should save the kid", ->
      expect(scope.activeKid).toBeUndefined()
      expect(scope.isOpen).toBeFalsy()
      return

  return

