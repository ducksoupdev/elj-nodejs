###
Created by levym on 19/03/14.
###
"use strict"
describe "keyPeopleController", ->
  keyPeopleController = undefined
  scope = undefined
  mockSessionService = jasmine.createSpyObj("sessionService", [
    "getProperty"
    "refresh"
  ])
  mockSessionService.getProperty.andCallFake (name) ->
    "12345"

  mockKeyPeopleService = jasmine.createSpyObj("keyPeopleService", ["getKeyPeople", "getKeyPerson", "save"])
  mockKeyPeopleService.getKeyPeople.andCallFake (settingId, callback) ->
    callback [
      {
        settingId: "12345"
        name: "Test 1"
      }
      {
        settingId: "12345"
        name: "Test 2"
      }
      {
        settingId: "12345"
        name: "Test 3"
      }
    ]
    return

  mockKeyPeopleService.getKeyPerson.andCallFake (keyPersonId, callback) ->
    callback {
      keyPersonId: keyPersonId,
      settingId: "12345",
      name: "Test 1"
    }
    return

  mockKeyPeopleService.save.andCallFake (keyPerson, callback) ->
    callback keyPerson
    return

  beforeEach module("settingApp")
  beforeEach module(($provide) ->
    $provide.value "sessionService", mockSessionService
    $provide.value "keyPeopleService", mockKeyPeopleService
    return
  )
  beforeEach inject(($controller, $rootScope) ->
    scope = $rootScope.$new()
    keyPeopleController = $controller("KeyPeopleController",
      $scope: scope
    )
    return
  )
  it "Should load the key people for the setting", ->
    expect(scope.keyPeople.length).toBe 3
    return

  describe "Editing a key person", ->
    keyPerson = undefined
    beforeEach ->
      keyPerson =
        keyPersonId: "12345"
        name: "Test 1"

      scope.editKeyPerson keyPerson
      return

    it "Should set the modal to open", ->
      expect(scope.isOpen).toBeTruthy()
      return

    return

  describe "Saving a key person", ->
    keyPerson = undefined
    beforeEach ->
      keyPerson =
        keyPersonId: "12345"
        name: "Test 1"

      scope.save keyPerson
      return

    it "Should save the key person", ->
      expect(scope.activeKeyPerson).toBeUndefined()
      expect(scope.isOpen).toBeFalsy()
      return

  return

