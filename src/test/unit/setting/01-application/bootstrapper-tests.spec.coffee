###
Created by levym on 14/03/14.
###
"use strict"
describe "Bootstrapper", ->
  beforeEach module("settingApp")
  routeService = null
  beforeEach inject(($injector) ->
    routeService = $injector.get("$route")
    return
  )
  it "Should return the home route", ->
    expect(routeService.routes["/home"].controller).toEqual "HomeController"
    return

  it "Should return the key people route", ->
    expect(routeService.routes["/keyPeople"].controller).toEqual "KeyPeopleController"
    return

  it "Should return the kids route", ->
    expect(routeService.routes["/kids"].controller).toEqual "KidsController"
    return

  return

