###
Created by levym on 14/03/14.
###
"use strict"
describe "kidsService", ->
  kidsService = undefined
  settingId = "80568699-8CB3-436D-BD10-61E2E89CDA2C"
  kidId = "A8E257F6-0649-0F45-A0A2-D275279B7384"
  kidsUri = "http://localhost/api/v1/kids"
  kidUri = "http://localhost/api/v1/kid"
  mockRuntimeService = jasmine.createSpyObj("runtimeService", ["useLiveData"])
  $httpBackend = undefined
  beforeEach module("settingApp")
  beforeEach module(($provide) ->
    $provide.value "eljRuntime", mockRuntimeService
    globalServiceUris =
      kidsUri: kidsUri + "/:settingId"
      kidUri: kidUri + "/:kidId"

    $provide.value "globalServiceUris", globalServiceUris
    return
  )
  beforeEach inject(($injector) ->
    $httpBackend = $injector.get("$httpBackend")
    kidsService = $injector.get("kidsService")
    return
  )
  beforeEach ->
    @addMatchers toEqualData: (expect) ->
      angular.equals expect, @actual

    return

  describe "when in http mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        true

      return

    afterEach ->
      $httpBackend.verifyNoOutstandingExpectation()
      $httpBackend.verifyNoOutstandingRequest()
      return

    it "should return kids details from the server", ->
      kids = [
        {
          settingId: "123Test"
          name: "Test kid 1"
        }
        {
          settingId: "123Test"
          name: "Test kid 2"
        }
        {
          settingId: "123Test"
          name: "Test kid 3"
        }
      ]
      $httpBackend.expect("GET", kidsUri + "/" + settingId).respond kids
      kidsService.getKids settingId, (kidsDetail) ->
        expect(kidsDetail).toEqualData kids
        return

      $httpBackend.flush()
      return

    it "should return kid details from the server", ->
      kid =
        settingId: "123Test"
        name: "Test kid 1"

      $httpBackend.expect("GET", kidUri + "/" + kidId).respond kid
      kidsService.getKid kidId, (kidDetail) ->
        expect(kidDetail).toEqualData kid
        return

      $httpBackend.flush()
      return

    it "Should save the kid to the server", ->
      kid =
        kidId: "12345"
        settingId: "123Test"
        name: "Test key person 1"

      $httpBackend.expect("POST", kidUri + "/" + kid.kidId).respond kid
      kidsService.save kid, (kidDetail) ->
        expect(kidDetail).toEqualData(kid)
        return

      $httpBackend.flush()
      return

    return

  describe "when in file mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        false

      return

    describe "if setting id is not recognised", ->
      it "should return no results", ->
        settingId = "0FB4EB58-B6FC-481A-A51B-7BD02DF82893"
        kidsService.getKids settingId, (kidsDetail) ->
          expect(kidsDetail).toEqual []
          return

        return

      return

    it "should return kids details from a local resource", ->
      expectedKidsDetail = window.app.setting.testKids().getKids(settingId)
      kidsService.getKids settingId, (kidsDetail) ->
        expect(kidsDetail).toEqualData expectedKidsDetail
        return

      return

    it "should return kid details from a local resource", ->
      expectedKidDetail = window.app.setting.testKids().getKid(kidId)
      kidsService.getKid kidId, (kidDetail) ->
        expect(kidDetail).toEqualData expectedKidDetail
        return

      return

    return

  return

