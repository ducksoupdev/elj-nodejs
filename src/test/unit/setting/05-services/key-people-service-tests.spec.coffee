###
Created by levym on 14/03/14.
###
"use strict"
describe "keyPeopleService", ->
  keyPeopleService = undefined
  settingId = "80568699-8CB3-436D-BD10-61E2E89CDA2C"
  keyPersonId = "A8E257F6-0649-0F45-A0A2-D275279B7384"
  keyPeopleUri = "http://localhost/api/v1/keyPeople"
  keyPersonUri = "http://localhost/api/v1/keyPerson"
  mockRuntimeService = jasmine.createSpyObj("runtimeService", ["useLiveData"])
  $httpBackend = undefined
  beforeEach module("settingApp")
  beforeEach module(($provide) ->
    $provide.value "eljRuntime", mockRuntimeService
    globalServiceUris =
      keyPeopleUri: keyPeopleUri + "/:settingId"
      keyPersonUri: keyPersonUri + "/:keyPersonId"

    $provide.value "globalServiceUris", globalServiceUris
    return
  )
  beforeEach inject(($injector) ->
    $httpBackend = $injector.get("$httpBackend")
    keyPeopleService = $injector.get("keyPeopleService")
    return
  )
  beforeEach ->
    @addMatchers toEqualData: (expect) ->
      angular.equals expect, @actual

    return

  describe "when in http mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        true

      return

    afterEach ->
      $httpBackend.verifyNoOutstandingExpectation()
      $httpBackend.verifyNoOutstandingRequest()
      return

    it "should return key people details from the server", ->
      keyPeople = [
        {
          settingId: "123Test"
          name: "Test key person 1"
        }
        {
          settingId: "123Test"
          name: "Test key person 2"
        }
        {
          settingId: "123Test"
          name: "Test key person 3"
        }
      ]
      $httpBackend.expect("GET", keyPeopleUri + "/" + settingId).respond keyPeople
      keyPeopleService.getKeyPeople settingId, (keyPeopleDetail) ->
        expect(keyPeopleDetail).toEqualData keyPeople
        return

      $httpBackend.flush()
      return

    it "should return key person details from the server", ->
      keyPerson =
        settingId: "123Test"
        name: "Test key person 1"

      $httpBackend.expect("GET", keyPersonUri + "/" + keyPersonId).respond keyPerson
      keyPeopleService.getKeyPerson keyPersonId, (keyPersonDetail) ->
        expect(keyPersonDetail).toEqualData keyPerson
        return

      $httpBackend.flush()
      return

    it "Should save the key person to the server", ->
      keyPerson =
        keyPersonId: "12345"
        settingId: "123Test"
        name: "Test key person 1"

      $httpBackend.expect("POST", keyPersonUri + "/" + keyPerson.keyPersonId).respond keyPerson
      keyPeopleService.save keyPerson, (keyPersonDetail) ->
        expect(keyPersonDetail).toEqualData(keyPerson)
        return

      $httpBackend.flush()
      return

    return

  describe "when in file mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        false

      return

    describe "if setting id is not recognised", ->
      it "should return no results", ->
        settingId = "0FB4EB58-B6FC-481A-A51B-7BD02DF82893"
        keyPeopleService.getKeyPeople settingId, (keyPeopleDetail) ->
          expect(keyPeopleDetail).toEqual []
          return

        return

      return

    it "should return key people details from a local resource", ->
      expectedKeyPeopleDetail = window.app.setting.testKeyPeople().getKeyPeople(settingId)
      keyPeopleService.getKeyPeople settingId, (keyPeopleDetail) ->
        expect(keyPeopleDetail).toEqualData expectedKeyPeopleDetail
        return

      return

    it "should return key person details from a local resource", ->
      expectedKeyPersonDetail = window.app.setting.testKeyPeople().getKeyPerson(keyPersonId)
      keyPeopleService.getKeyPerson keyPersonId, (keyPersonDetail) ->
        expect(keyPersonDetail).toEqualData expectedKeyPersonDetail
        return

      return

    return

  return

