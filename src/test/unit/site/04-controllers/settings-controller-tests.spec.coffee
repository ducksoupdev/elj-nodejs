###
Created by levym on 19/03/14.
###
"use strict"
describe "settingsController", ->
  settingsController = undefined
  scope = undefined
  mockSessionService = jasmine.createSpyObj("sessionService", [
    "getProperty"
    "refresh"
  ])
  mockSessionService.getProperty.andCallFake (name) ->
    "12345"

  mockSettingsService = jasmine.createSpyObj("settingsService", ["getSettings", "save"])
  mockSettingsService.getSettings.andCallFake (callback) ->
    callback [
      {
        settingId: "12345"
        name: "Test 1"
      }
      {
        settingId: "12345"
        name: "Test 2"
      }
      {
        settingId: "12345"
        name: "Test 3"
      }
    ]
    return

  mockSettingsService.save.andCallFake (setting, callback) ->
    callback setting
    return

  beforeEach module("siteApp")
  beforeEach module(($provide) ->
    $provide.value "sessionService", mockSessionService
    $provide.value "settingsService", mockSettingsService
    return
  )
  beforeEach inject(($controller, $rootScope) ->
    scope = $rootScope.$new()
    settingsController = $controller("SettingsController",
      $scope: scope
    )
    return
  )
  it "Should load the settings", ->
    expect(scope.settings.length).toBe 3
    return

  describe "Editing a setting", ->
    setting = undefined
    beforeEach ->
      setting =
        settingId: "12345"
        name: "Test 1"

      scope.editSetting setting
      return

    it "Should set the modal to open", ->
      expect(scope.isOpen).toBeTruthy()
      return

    return

  describe "Saving a setting", ->
    setting = undefined
    beforeEach ->
      setting =
        settingId: "12345"
        name: "Test 1"

      scope.save setting
      return

    it "Should save the setting", ->
      expect(scope.activeSetting).toBeUndefined()
      expect(scope.isOpen).toBeFalsy()
      return

  return

