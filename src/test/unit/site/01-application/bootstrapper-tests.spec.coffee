###
Created by levym on 14/03/14.
###
"use strict"
describe "Bootstrapper", ->
  beforeEach module("siteApp")
  routeService = null
  beforeEach inject(($injector) ->
    routeService = $injector.get("$route")
    return
  )
  it "Should return the home route", ->
    expect(routeService.routes["/home"].controller).toEqual "HomeController"
    return

  it "Should return the setting route", ->
    expect(routeService.routes["/settings"].controller).toEqual "SettingsController"
    return

  return

