###
Created by levym on 28/03/2014.
###
"use strict"
describe "cssModal", ->
  beforeEach module("global")
  scope = undefined
  $compile = undefined
  $timeout = undefined
  element = undefined
  compiled = undefined
  html = "<css-modal is-open='isOpen'><div class='modal-inner'><header></header><div class='modal-content'></div><footer></footer></div></css-modal>"
  beforeEach inject((_$rootScope_, _$compile_) ->
    scope = _$rootScope_.$new()
    $compile = _$compile_
    element = angular.element(html)
    compiled = $compile(element)(scope)
    scope.isOpen = false
    scope.$digest()
    return
  )
  it "Should not be displayed initially", ->
    expect(element.attr("class")).not.toContain "is-active"
    return

  describe "Opening the modal", ->
    beforeEach ->
      scope.isOpen = true
      scope.$apply()
      return

    it "Should request focus on the element", ->
      expect(element.attr("class")).toContain "is-active"
      return

    describe "Closing the modal", ->
      beforeEach ->
        scope.isOpen = false
        scope.$apply()
        return

      it "Should hide the modal", ->
        expect(element.attr("class")).not.toContain "is-active"
        return

      return

    return

  return

