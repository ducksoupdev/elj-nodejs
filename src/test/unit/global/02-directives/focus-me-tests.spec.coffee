###
Created by levym on 14/03/14.
###
"use strict"
describe "focusMe", ->
  beforeEach module("global")
  scope = undefined
  $compile = undefined
  $timeout = undefined
  element = undefined
  compiled = undefined
  html = "<input id='test' data-focus-me='name' data-focus-class='focused'>"
  beforeEach inject((_$rootScope_, _$compile_) ->
    scope = _$rootScope_.$new()
    $compile = _$compile_
    element = angular.element(html)
    compiled = $compile(element)(scope)
    scope.$digest()
    return
  )
  it "Should not have focus initially", ->
    expect(element.hasClass("focused")).toBeFalsy()
    return

  it "Should request focus on the element", ->
    scope.name = true
    scope.$apply()
    expect(element.hasClass("focused")).toBeTruthy()
    return

  return

