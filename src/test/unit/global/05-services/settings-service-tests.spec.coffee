###
Created by levym on 14/03/14.
###
"use strict"
describe "settingsService", ->
  settingsService = undefined
  settingId = "8D5FEB39F77D4F3F"
  testUri = "http://localhost/api/v1/settings"
  mockRuntimeService = jasmine.createSpyObj("runtimeService", ["useLiveData"])
  $httpBackend = undefined
  beforeEach module("global")
  beforeEach module(($provide) ->
    $provide.value "eljRuntime", mockRuntimeService
    globalServiceUris = settingsUri: testUri + "/:settingId"
    $provide.value "globalServiceUris", globalServiceUris
    return
  )
  beforeEach inject(($injector) ->
    $httpBackend = $injector.get("$httpBackend")
    settingsService = $injector.get("settingsService")
    return
  )
  beforeEach ->
    @addMatchers toEqualData: (expect) ->
      angular.equals expect, @actual

    return

  describe "when in http mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        true

      return

    afterEach ->
      $httpBackend.verifyNoOutstandingExpectation()
      $httpBackend.verifyNoOutstandingRequest()
      return

    it "should return setting details from the server", ->
      settingDetails =
        settingId: "123Test"
        name: "Test login"

      $httpBackend.expect("GET", testUri + "/" + settingId).respond settingDetails
      settingsService.getSetting settingId, (serverSettingDetail) ->
        expect(serverSettingDetail).toEqualData settingDetails
        return

      $httpBackend.flush()
      return

    it "should return settings from the server", ->
      settingsArray = [
        { settingId: "12Test", name: "Test setting 1" },
        { settingId: "123Test", name: "Test setting 2" }
      ]

      $httpBackend.expect("GET", testUri).respond settingsArray
      settingsService.getSettings (serverSettings) ->
        expect(serverSettings).toEqualData settingsArray
        return

      $httpBackend.flush()
      return

    return

  describe "when in file mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        false

      return

    describe "if setting id is not recognised", ->
      it "should return no results", ->
        settingId = "0FB4EB58-B6FC-481A-A51B-7BD02DF82893"
        settingsService.getSetting settingId, (settingDetail) ->
          expect(settingDetail).toBeNull()
          return

        return

      return

    it "should return setting details from a local resource", ->
      expectedSettingDetail = window.app.elj.testSettings().getSetting(settingId)
      settingsService.getSetting settingId, (serverSettingDetail) ->
        expect(serverSettingDetail).toEqualData expectedSettingDetail
        return

      return

    it "should return settings from a local resource", ->
      expectedSettings = window.app.elj.testSettings().getSettings()
      settingsService.getSettings (serverSettings) ->
        expect(serverSettings).toEqualData expectedSettings
        return

      return

    return

  return

