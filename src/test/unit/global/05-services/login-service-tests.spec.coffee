###
Created by levym on 14/03/14.
###
"use strict"
describe "loginService", ->
  loginService = undefined
  loginId = "0FFED851-8ADE-ECB9-3B15-DB6C3C8C5A1A"
  testUri = "http://localhost/api/v1/login"
  mockRuntimeService = jasmine.createSpyObj("runtimeService", ["useLiveData"])
  $httpBackend = undefined
  beforeEach module("global")
  beforeEach module(($provide) ->
    $provide.value "eljRuntime", mockRuntimeService
    globalServiceUris = loginUri: testUri + "/:loginId"
    $provide.value "globalServiceUris", globalServiceUris
    return
  )
  beforeEach inject(($injector) ->
    $httpBackend = $injector.get("$httpBackend")
    loginService = $injector.get("loginService")
    return
  )
  beforeEach ->
    @addMatchers toEqualData: (expect) ->
      angular.equals expect, @actual

    return

  describe "when in http mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        true

      return

    afterEach ->
      $httpBackend.verifyNoOutstandingExpectation()
      $httpBackend.verifyNoOutstandingRequest()
      return

    it "should return login details from the server", ->
      loginDetails =
        loginId: "123Test"
        name: "Test login"

      $httpBackend.expect("GET", testUri + "/" + loginId).respond loginDetails
      loginService.getLogin loginId, (serverLoginDetail) ->
        expect(serverLoginDetail).toEqualData loginDetails
        return

      $httpBackend.flush()
      return

    return

  describe "when in file mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        false

      return

    describe "if login id is not recognised", ->
      it "should return no results", ->
        loginId = "0FB4EB58-B6FC-481A-A51B-7BD02DF82893"
        loginService.getLogin loginId, (loginDetail) ->
          expect(loginDetail).toBeNull()
          return

        return

      return

    it "should return login details from a local resource", ->
      expectedLoginDetail = window.app.elj.testLogin().getLogin(loginId)
      loginService.getLogin loginId, (serverLoginDetail) ->
        expect(serverLoginDetail).toEqualData expectedLoginDetail
        return

      return

    return

  return

