###
Created by levym on 14/03/14.
###
"use strict"
describe "sessionService", ->
  sessionService = undefined
  testUri = "http://localhost/api/v1/session"
  mockRuntimeService = jasmine.createSpyObj("runtimeService", ["useLiveData"])
  $httpBackend = undefined
  beforeEach module("global")
  beforeEach module(($provide) ->
    $provide.value "eljRuntime", mockRuntimeService
    globalServiceUris = sessionUri: testUri
    $provide.value "globalServiceUris", globalServiceUris
    return
  )
  beforeEach inject(($injector) ->
    $httpBackend = $injector.get("$httpBackend")
    sessionService = $injector.get("sessionService")
    return
  )
  beforeEach ->
    @addMatchers toEqualData: (expect) ->
      angular.equals expect, @actual

    return

  describe "when in http mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        true

      return

    afterEach ->
      $httpBackend.verifyNoOutstandingExpectation()
      $httpBackend.verifyNoOutstandingRequest()
      return

    it "should return session details from the server", ->
      sessionDetails =
        loggedIn: true
        settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
        loginId: "0FFED851-8ADE-ECB9-3B15-DB6C3C8C5A1A"

      $httpBackend.expect("GET", testUri).respond sessionDetails
      sessionService.refresh()
      $httpBackend.flush()
      expect(sessionService.session).toEqualData sessionDetails
      expect(sessionService.getProperty("settingId")).toEqual "80568699-8CB3-436D-BD10-61E2E89CDA2C"
      return

    return

  describe "when in file mode", ->
    beforeEach ->
      mockRuntimeService.useLiveData.andCallFake ->
        false

      return

    it "should return clinic details from a local resource", ->
      sessionDetails =
        loggedIn: true
        settingId: "80568699-8CB3-436D-BD10-61E2E89CDA2C"
        loginId: "0FFED851-8ADE-ECB9-3B15-DB6C3C8C5A1A"

      sessionService.refresh()
      expect(sessionService.session).toEqualData sessionDetails
      expect(sessionService.getProperty("loginId")).toEqual "0FFED851-8ADE-ECB9-3B15-DB6C3C8C5A1A"
      return

    return

  return

