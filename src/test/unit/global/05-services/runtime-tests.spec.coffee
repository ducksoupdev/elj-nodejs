###
Created by levym on 17/03/14.
###
"use strict"
describe "eljRuntime", ->
  runtimeService = undefined
  mockLocationService = jasmine.createSpyObj("$location", [
    "protocol"
    "host"
    "port"
  ])
  beforeEach module("global")
  beforeEach module(($provide) ->
    $provide.value "$location", mockLocationService
    return
  )
  beforeEach inject(($injector) ->
    runtimeService = $injector.get("eljRuntime")
    return
  )
  describe "When using live data", ->
    beforeEach ->
      mockLocationService.protocol.andCallFake ->
        "http"

      mockLocationService.host.andCallFake ->
        "localhost"

      mockLocationService.port.andCallFake ->
        8080

      return

    it "should return true when using live data", ->
      expect(runtimeService.useLiveData()).toBeTruthy()
      return

    return

  describe "when in file mode", ->
    beforeEach ->
      mockLocationService.protocol.andCallFake ->
        "file"

      mockLocationService.host.andCallFake ->
        "localhost"

      mockLocationService.port.andCallFake ->
        8080

      return

    it "should return false when using live data", ->
      expect(runtimeService.useLiveData()).toBeFalsy()
      return

    return

  return

