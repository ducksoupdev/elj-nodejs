###
Created by levym on 14/03/14.
###
"use strict"
describe "flashService", ->
  flashService = undefined
  beforeEach module("global")
  beforeEach inject(($injector) ->
    flashService = $injector.get("flashService")
    return
  )
  describe "The flash methods", ->
    describe "The add method", ->
      beforeEach ->
        flashService.add "INFO", "This is an info message"
        return

      it "Should add a flash message", ->
        expect(flashService.getMessages().length).toEqual 1
        return

      describe "The clear method", ->
        beforeEach ->
          flashService.clear()
          return

        it "Should clear the flash messages", ->
          expect(flashService.getMessages().length).toEqual 0
          return

        return

      return

    return

  return

