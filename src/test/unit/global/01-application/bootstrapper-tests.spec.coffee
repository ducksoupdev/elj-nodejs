###
Created by levym on 14/03/14.
###
"use strict"
describe "Bootstrapper", ->
  beforeEach module("global")
  globalServiceUris = null
  beforeEach inject(($injector) ->
    globalServiceUris = $injector.get("globalServiceUris")
    return
  )
  it "Should return the sessionUri", ->
    expect(globalServiceUris.sessionUri).toEqual "http://localhost:3001/api/v1/session"
    return

  it "Should return the settingsUri", ->
    expect(globalServiceUris.settingsUri).toEqual "http://localhost:3001/api/v1/settings/:settingId"
    return

  it "Should return the loginUri", ->
    expect(globalServiceUris.loginUri).toEqual "http://localhost:3001/api/v1/login/:loginId"
    return

  it "Should return the keyPeopleUri", ->
    expect(globalServiceUris.keyPeopleUri).toEqual "http://localhost:3001/api/v1/keyPeople/:settingId"
    return

  it "Should return the keyPersonUri", ->
    expect(globalServiceUris.keyPersonUri).toEqual "http://localhost:3001/api/v1/keyPerson/:keyPersonId"
    return

  return

