###
Created by levym on 14/03/14.
###
"use strict"
describe "arraySearch", ->
  beforeEach module("global")
  arraySearch = null
  beforeEach inject(($injector) ->
    arraySearch = $injector.get("arraySearch")
    return
  )
  testArray = null
  beforeEach ->
    testArray = [
      {
        Id: 1
        name: "Cheese"
      }
      {
        Id: 2
        name: "Ham"
      }
      {
        Id: 3
        name: "Onions"
      }
      {
        Id: 4
        name: "Ham"
      }
    ]
    return

  describe "when searching for a single match", ->
    it "Should return null if the array to search is null", ->
      item = arraySearch.single(null, (item) ->
        item.name is "Ham"
      )
      expect(item).toBeNull()
      return

    it "returns the first item that matches", ->
      item = arraySearch.single(testArray, (item) ->
        item.name is "Ham"
      )
      expect(item).toBe testArray[1]
      return

    it "returns null if no match found", ->
      item = arraySearch.single(testArray, (item) ->
        item.name is "Hample"
      )
      expect(item).toBe null
      return

    return

  describe "when searching for all matches", ->
    it "Should return null if the array to search is null", ->
      item = arraySearch.all(null, (item) ->
        item.name is "Ham"
      )
      expect(item).toBeNull()
      return

    it "returns all matching elements", ->
      item = arraySearch.all(testArray, (item) ->
        item.name is "Ham"
      )
      expect(item.length).toBe 2
      expect(item[0]).toBe testArray[1]
      expect(item[1]).toBe testArray[3]
      return

    it "returns an empty array if there are no matches", ->
      item = arraySearch.all(testArray, (item) ->
        item.name is "Hample"
      )
      expect(item instanceof Array).toBeTruthy()
      expect(item.length).toBe 0
      return

    return

  return

