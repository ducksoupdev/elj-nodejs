// Generated by CoffeeScript 1.7.1

/*
Created by levym on 25/10/13.
 */

(function() {
  var crypto, shasum;

  crypto = require("crypto");

  shasum = crypto.createHash("sha1");

  shasum.update(process.argv[2]);

  console.log(shasum.digest("hex"));

}).call(this);

//# sourceMappingURL=create_password.map
