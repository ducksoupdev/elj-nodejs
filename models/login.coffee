###
Created by levym on 04/11/13.
###
"use strict"
module.exports = (sequelize, DataTypes) ->
  Login = sequelize.define("Login",
    loginId:
      type: DataTypes.UUIDV4
      primaryKey: true

    username: DataTypes.STRING
    password: DataTypes.STRING
    usernameIsEmail: DataTypes.BOOLEAN
    verificationCode: DataTypes.STRING
    verifiedDate: DataTypes.DATE
    entity: DataTypes.STRING # Setting, Site, KeyPerson, Parent
    entityId: DataTypes.INTEGER
    loginCount: DataTypes.INTEGER
    lastLoginDate: DataTypes.DATE
    autoLogin: DataTypes.STRING
    settingId: DataTypes.STRING
  )
  Login
