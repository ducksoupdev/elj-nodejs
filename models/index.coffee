###
Created by levym on 14/03/14.
###
"use strict"
fs = require("fs")
path = require("path")
Sequelize = require("sequelize")
__ = require("underscore")._
config = require("../config")
sequelize = new Sequelize(config.db.database, config.db.username, config.db.password, config.db.options)
db = {}

fs.readdirSync(__dirname).filter((file) ->
  (file.indexOf(".") isnt 0) and (file isnt "index.js") and (file.slice(-3) is ".js")
).forEach (file) ->
  model = sequelize.import(path.join(__dirname, file))
  db[model.name] = model
  return

Object.keys(db).forEach (modelName) ->
  db[modelName].options.associate db  if db[modelName].options.hasOwnProperty("associate")
  return

module.exports = __.extend(
  sequelize: sequelize
  Sequelize: Sequelize
, db)
