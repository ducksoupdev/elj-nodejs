###
Created by levym on 04/11/13.
###
"use strict"
module.exports = (sequelize, DataTypes) ->
  Setting = sequelize.define("Setting",
    settingId:
      type: DataTypes.UUIDV4
      primaryKey: true

    name: DataTypes.STRING
    phone: DataTypes.STRING
    urn: DataTypes.STRING
    address: DataTypes.TEXT
    contactName: DataTypes.STRING
    type: DataTypes.STRING
    source: DataTypes.STRING
    researchContact: DataTypes.BOOLEAN
    shortUrl: DataTypes.STRING
    status: DataTypes.STRING # Active, Disabled etc
    loginId: DataTypes.STRING
  )
  Setting
