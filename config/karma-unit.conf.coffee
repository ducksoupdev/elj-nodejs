"use strict"
module.exports = (config) ->
  config.set
    basePath: "../src/"
    files: [
      "bower_components/angular/angular.js"
      "bower_components/angular-cookies/angular-cookies.js"
      "bower_components/angular-mocks/angular-mocks.js"
      "bower_components/angular-resource/angular-resource.js"
      "bower_components/angular-route/angular-route.js"
      "global/**/*.js"
      "site/**/*.js"
      "setting/**/*.js"
      "keyPerson/**/*.js"
      "carer/**/*.js"
      "test/unit/**/*.spec.js"
    ]
    
    # list of files to exclude
    exclude: [
      "global/**/*.min.js"
      "site/**/*.min.js"
      "setting/**/*.min.js"
      "keyPerson/**/*.min.js"
      "carer/**/*.min.js"
    ]
    preprocessors:
      "global/**/!(*.test).js": ["coverage"]
      "site/**/!(*.test).js": ["coverage"]
      "setting/**/!(*.test).js": ["coverage"]
      "keyPerson/**/!(*.test).js": ["coverage"]
      "carer/**/!(*.test).js": ["coverage"]

    reporters: [
      "progress"
      "coverage"
    ]
    loggers: [type: "console"]
    coverageReporter:
      type: "html"
      dir: "../results/karma-coverage/unit/"

    
    # enable / disable colors in the output (reporters and logs)
    colors: true
    
    # level of logging
    # possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_WARN
    autoWatch: true
    frameworks: ["jasmine"]
    browsers: ["Chrome"]
    
    # If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000
    plugins: [
      "karma-junit-reporter"
      "karma-chrome-launcher"
      "karma-firefox-launcher"
      "karma-jasmine"
      "karma-coverage"
    ]
    junitReporter:
      outputFile: "test_out/unit.xml"
      suite: "unit"

    
    # Continuous Integration mode
    # if true, it capture browsers, run tests and exit
    singleRun: false

  return
