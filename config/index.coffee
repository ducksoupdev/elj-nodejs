###
Created by levym on 04/03/14.
###
"use strict"
process.env.NODE_ENV = "development" # or 'production' or 'test'
config = ->
  switch process.env.NODE_ENV
    when "test"
      url:
        address: "http://localhost:3001"

      db:
        username: "root"
        password: "root"
        database: "elj"
        options:
          dialect: "sqlite"
          native: true

      logger:
        api: "logs/api_test.log"
        exception: "logs/exceptions_test.log"
        express: "logs/express_test.log"

      smtp:
        host: "relay.plus.net" # hostname
        port: 25
        auth:
          user: "vikkiandmatt+matthew.levy"
          pass: "Lizzie46"

      cookieSecret: "Wh4j3MPrL6y0jF3tueHk"
    when "production"
      url:
        address: "http://dev.earlylearnningjourneys.co.uk"

      db:
        username: "root"
        password: "Gr3g0r5"
        database: "elj"
        options:
          dialect: "mysql"
          native: true

      logger:
        api: "logs/express.log"
        exception: "logs/exceptions.log"
        express: "logs/express.log"

      smtp:
        host: "localhost" # hostname
        port: 25

      cookieSecret: "Wh4j3MPrL6y0jF6tueHk"
    else
      url:
        address: "http://localhost:3001"

      db:
        username: "root"
        password: "root"
        database: "elj"
        options:
          dialect: "sqlite"
          storage: "data/elj.sqlite3"
          native: true

      logger:
        api: "logs/express.log"
        exception: "logs/exceptions.log"
        express: "logs/express.log"

      smtp:
        host: "relay.plus.net" # hostname
        port: 25
        auth:
          user: "vikkiandmatt+matthew.levy"
          pass: "Lizzie46"

      cookieSecret: "Wh4j3MPrL6y0jF1tueHk"
()
module.exports = config
