###
Created by levym on 05/03/14.
###
"use strict"
module.exports = (grunt) ->
  
  # load all grunt tasks matching the `grunt-*` pattern
  require("load-grunt-tasks") grunt
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")
    mochacli:
      options:
        require: ["expectations"]
        reporter: "spec"
        ui: "bdd"
        recursive: true
        debug: true

      unit:
        options:
          files: "test/mocha/unit/*.spec.js"

      int:
        options:
          files: "test/mocha/integration/*.spec.js"

    watch:
      "mocha-unit":
        files: [
          "test/unit/*.js"
          "models/*.js"
          "util/*.js"
        ]
        tasks: ["mochacli:unit"]

      "mocha-int":
        files: [
          "test/integration/*.js"
          "handlers/*.js"
          "models/*.js"
        ]
        tasks: ["mochacli:int"]

      "karma-unit":
        files: [
          "src/test/unit/**/*.js"
          "src/global/**/*.js"
          "src/setting/**/*.js"
          "src/site/**/*.js"
        ]
        tasks: ["karma:unit"]

      "compass-dev":
        files: ["src/sass/*.scss"]
        tasks: ["compass-dev"]

    env:
      dev:
        NODE_ENV: "development"

      test:
        NODE_ENV: "test"

      prod:
        NODE_ENV: "production"

    shell:
      "mocha-coverage":
        command: "istanbul cover -v --dir coverage/mocha -x src/** node_modules/mocha/bin/_mocha -- -R spec test/mocha/**/*.js"

    bgShell:
      "run-test-server":
        cmd: "node index.js"
        bg: true

    clean:
      css: ["src/stylesheets"]
      build: ["build"]

    compass:
      clean:
        options:
          clean: true

      dist:
        options:
          sassDir: "src/sass"
          cssDir: "build/stylesheets"
          environment: "production"
          relativeAssets: true
          outputStyle: "compressed"

      dev:
        options:
          sassDir: "src/sass"
          cssDir: "src/stylesheets"
          relativeAssets: true

    concat:
      options:
        process: true

      dist:
        src: [
          "src/bower_components/angular/angular.min.js"
          "src/bower_components/angular-route/angular-route.min.js"
          "src/bower_components/angular-resource/angular-resource.min.js"
          "src/bower_components/angular-cookies/angular-cookies.min.js"
          "src/bower_components/angular-ui-bootstrap/dist/ui-bootstrap-tpls-0.10.0.min.js"
        ]
        dest: "build/bower_components/combined.min.js"

    uglify:
      options:
        compress: true
        sourceMap: false

      setting:
        files:
          "build/setting/setting.min.js": [
            "src/global/**/*.js"
            "src/setting/**/*.js"
            "!src/setting/setting.min.js"
            "!src/global/static-data/*"
            "!src/setting/static-data/*"
          ]

      site:
        files:
          "build/site/site.min.js": [
            "src/global/**/*.js"
            "src/site/**/*.js"
            "!src/site/site.min.js"
            "!src/global/static-data/*"
            "!src/site/static-data/*"
          ]

    processhtml:
      sitedist:
        options:
          process: true

        files:
          "build/site.html": ["src/site.html"]

      settingdist:
        options:
          process: true

        files:
          "build/setting.html": ["src/setting.html"]

    copy:
      html:
        files: [
          expand: true
          cwd: "src/"
          src: ["*.html"]
          dest: "build/"
        ]

      css:
        files: [
          expand: true
          cwd: "src/stylesheets/"
          src: ["**"]
          dest: "build/stylesheets/"
        ]

      images:
        files: [
          expand: true
          cwd: "src/img/"
          src: ["**"]
          dest: "build/img/"
        ]

      libs:
        files: [
          {
            expand: true
            flatten: true
            src: ["src/bower_components/html5shiv/dist/html5shiv*"]
            dest: "build/bower_components/html5shiv/dist/"
            rename: (dest, src) ->
              dest + src.replace(/\.js$/, ".min.js")
          }
          {
            expand: true
            cwd: "src/"
            src: [
              "bower_components/respond/**/*.min.js"
            ]
            dest: "build/"
          }
        ]

      js:
        files: [
          expand: true
          cwd: "src/"
          src: [
            "**/*.min.*"
            "!bower_components/**/*.min.*"
          ]
          dest: "build/"
        ]

      partials:
        files: [
          expand: true
          cwd: "src/"
          src: ["**/partials/*.html"]
          dest: "build/"
        ]

    karma:
      unit:
        configFile: "config/karma-unit.conf.js"
        singleRun: true

      e2e:
        configFile: "config/karma-e2e.conf.js"
        singleRun: true

    jshint:
      options:
        reporter: require("jshint-stylish")
        ignores: ["src/bower_components/**"]
        jshintrc: true

      all: ["src/**/*.js"]

    coffee:
      globRoot:
        expand: true
        cwd: "."
        src: ["*.coffee"]
        dest: "."
        rename: (dest, src) ->
          dest + "/" + src.replace(/\.coffee$/, ".js")

      globSrc:
        expand: true
        cwd: "src/"
        src: ["**/*.coffee"]
        dest: "src/"
        rename: (dest, src) ->
          dest + "/" + src.replace(/\.coffee$/, ".js")


  grunt.registerTask "mocha-unit", [
    "env:test"
    "mochacli:unit"
  ]
  grunt.registerTask "mocha-int", [
    "env:test"
    "mochacli:int"
  ]
  grunt.registerTask "mocha-full", [
    "env:test"
    "mochacli:unit"
    "mochacli:int"
    "shell:mocha-coverage"
  ]
  grunt.registerTask "karma-unit", [
    "env:test"
    "karma:unit"
  ]
  grunt.registerTask "karma-server", [
    "env:test"
    "bgShell:run-test-server"
  ]
  grunt.registerTask "karma-e2e", ["karma:e2e"]
  grunt.registerTask "karma-full", [
    "env:test"
    "bgShell:run-test-server"
    "karma:unit"
    "karma:e2e"
  ]
  grunt.registerTask "watch-mocha-unit", ["watch:mocha-unit"]
  grunt.registerTask "watch-mocha-int", ["watch:mocha-int"]
  grunt.registerTask "watch-karma-unit", ["watch:karma-unit"]
  grunt.registerTask "watch-compass-dev", ["watch:compass-dev"]
  grunt.registerTask "mocha-coverage", ["shell:mocha-coverage"]
  grunt.registerTask "compass-clean", ["compass:clean"]
  grunt.registerTask "compass-dist", [
    "clean:css"
    "compass:clean"
    "compass:dist"
  ]
  grunt.registerTask "compass-dev", [
    "clean:css"
    "compass:clean"
    "compass:dev"
  ]
  grunt.registerTask "uglify-setting", ["uglify:setting"]
  grunt.registerTask "uglify-key-person", ["uglify:keyPerson"]
  grunt.registerTask "uglify-carer", ["uglify:carer"]
  grunt.registerTask "build-dev", [
    "jshint"
    "compass-dev"
  ]
  grunt.registerTask "build-prod", [
    "jshint"
    "clean:build"
    "compass-dist"
    "uglify"
    "concat"
    "copy"
    "processhtml"
  ]
  return
