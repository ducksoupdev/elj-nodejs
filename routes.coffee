###
Created by levym on 05/03/14.
###
"use strict"
setupRoutes = (app, routes) ->
  routes.forEach (route) ->
    switch (route.method)
      when "GET" then app.get(route.path, route.handler)
      when "POST" then app.post(route.path, route.handler)
      when "PUT" then app.put(route.path, route.handler)
      when "DELETE" then app.delete(route.path, route.handler)
      when "HEAD" then app.head(route.path, route.handler)

  return
module.exports = (app, handlers) ->
  for name of handlers
    routes = handlers[name]
    setupRoutes app, routes
  return
