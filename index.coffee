###
Created by levym on 05/03/14.
###
"use strict"
winston = require("winston")
readLine = require("readline")
server = require("./server")
config = require("./config")
db = require("./models")

# We will log normal api operations into api.log
console.log "starting logger..."
winston.add winston.transports.File,
  filename: config.logger.api

# We will log all uncaught exceptions into exceptions.log
winston.handleExceptions new winston.transports.File(filename: config.logger.exception)
if process.platform is "win32"
  rl = readLine.createInterface(
    input: process.stdin
    output: process.stdout
  )
  rl.on "SIGINT", ->
    process.emit "SIGINT"
    return

  rl.on "SIGTERM", ->
    process.emit "SIGTERM"
    return

process.on "SIGINT", ->
  console.log "Closing"
  db.sequelize.drop()  if process.env.NODE_ENV is "development" or process.env.NODE_ENV is "test"
  server.close()
  process.exit()
  return

process.on "SIGTERM", ->
  console.log "Closing"
  db.sequelize.drop()  if process.env.NODE_ENV is "development" or process.env.NODE_ENV is "test"
  server.close()
  process.exit()
  return

syncOptions = force: false
syncOptions.force = true  if process.env.NODE_ENV is "development" or process.env.NODE_ENV is "test"
db.sequelize.sync(syncOptions).complete (err) ->
  throw err  if err
  return

console.log "Successfully connected to %s database %s. Starting web server...", config.db.options.dialect, config.db.database
server.start()
console.log "Successfully started web server. Waiting for incoming connections..."
