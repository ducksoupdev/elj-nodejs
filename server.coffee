###
Created by levym on 05/03/14.
###
"use strict"

# functions for starting and stopping the application
start = ->
  require("./routes") app, handlerList.handlers
  port = process.env.PORT or 3001
  server = app.listen(port)
  console.log "Express server listening on port %d in %s mode", port, app.settings.env
  return
stop = ->
  console.log "Stopping express"
  app.close()
  return

# includes
express = require("express")
SequalizeStore = require("connect-session-sequelize")(express)
cons = require("consolidate")
fs = require("fs")
path = require("path")
config = require("./config")
db = require("./models")

# create an express app
app = express()
expressLogFile = fs.createWriteStream(config.logger.express,
  flags: "a"
)

# configure express app
app.configure ->
  # assign the mustache engine to .html files
  app.engine "html", cons.mustache
  app.set "view engine", "html"
  app.use express.logger(stream: expressLogFile)
  app.use express.favicon()
  app.use express.compress()
  app.use express.json()
  app.use express.urlencoded()
  app.use express.methodOverride()
  app.use express.cookieParser()
  app.use express.session(
    secret: config.cookieSecret
    cookie:
      httpOnly: false

    store: new SequalizeStore(db: db.sequelize)
  )
  app.use app.router
  return

app.configure "development", ->
  app.set "views", path.join(__dirname, "src")
  app.use express.static(path.join(__dirname, "src"))
  app.use express.errorHandler(
    dumpExceptions: true
    showStack: true
  )
  return

app.configure "test", ->
  app.set "views", path.join(__dirname, "src")
  app.use express.static(path.join(__dirname, "src"))
  app.use express.errorHandler(
    dumpExceptions: true
    showStack: true
  )
  return

app.configure "production", ->
  app.set "views", path.join(__dirname, "build")
  app.use express.static(path.join(__dirname, "build"))
  app.use express.errorHandler()
  return

handlerList = require("./handlers")
server = null
module.exports.start = start
module.exports.stop = stop
module.exports.app = app
