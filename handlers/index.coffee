###
Created by levym on 05/03/14.
###
"use strict"
fs = require("fs")
path = require("path")

handler = ->
  _handlers = {}
  fs.readdirSync(__dirname).filter((file) ->
    (file.indexOf(".") isnt 0) and (file isnt "index.js")
  ).forEach (file) ->
    handlerSpec = require(path.join(__dirname, file))
    _handlers[handlerSpec.name] = handlerSpec.handlers
    return

  handlers: _handlers
()
module.exports = handler
