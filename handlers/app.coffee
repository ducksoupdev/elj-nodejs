###
Created by levym on 05/03/14.
###
"use strict"
handler = ->
  _get = (req, res) ->
    res.render "index",
      title: "Early Learning Journeys"

    return

  _login = (req, res) ->
    res.render "login",
      title: "Please login"

    return

  _setting = (req, res) ->
    if not req.session.loggedIn or not req.session.settingId or not req.session.loginId
      res.redirect "/login"
    else
      res.render "setting",
        title: "Setting"

    return

  name: "app"
  handlers: [
    {
      method: "GET"
      path: "/"
      handler: _get
    }
    {
      method: "GET"
      path: "/login"
      handler: _login
    }
    {
      method: "GET"
      path: "/setting"
      handler: _setting
    }
  ]
()
module.exports = handler
