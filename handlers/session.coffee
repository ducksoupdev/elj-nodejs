###
Created by levym on 05/03/14.
###
"use strict"
handler = ->
  _session = (req, res) ->
    if req.session.loggedIn and req.session.settingId and req.session.loginId
      res.json
        loggedIn: req.session.loggedIn
        settingId: req.session.settingId
        loginId: req.session.loginId

    else
      res.json loggedIn: false
    return

  name: "session"
  handlers: [
    method: "GET"
    path: "/api/v1/session/details"
    handler: _session
  ]
()
module.exports = handler
